# QIIME2 using Docker image and Common Workflow Language (CWL) 

Start QIIME2 an as interactive mode (optional)  
  
`docker run -it qiime2/core:2019.10`

`pip install cwltool`

`adinasarapu$cwltool qiime2-tool.cwl qiime2-job.yml` 

Installing Community Plugins in [Docker](https://forum.qiime2.org/t/installing-community-plugins-in-docker/7106/1)  

====================================================================================  
`/Library/Python/2.7/site-packages/cwltool/__init__.py:17: CWLToolDeprecationWarning: 
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020.
Please upgrade your Python as the Python 2.7 version of cwltool won't be
maintained after that date.

  """,  category=CWLToolDeprecationWarning)
INFO /usr/local/bin/cwltool 1.0.20191225192155
INFO Resolved 'qiime2-tool.cwl' to 'file:///Users/adinasarapu/cwl/qiime2-tool.cwl'
INFO [job qiime2-tool.cwl] /private/tmp/docker_tmpVPULyc$ sh \
    /private/var/folders/wb/wdp0bzbn6vzb5h056gb1r9b40000gn/T/tmpkfOu9D/stgffd52469-9714-49b8-b4c1-80db8e8799c4/docker_qiime2.sh > /private/tmp/docker_tmpVPULyc/output.txt`  

INFO [job qiime2-tool.cwl] completed success  
{  
	"std_out": {  
	"checksum": "sha1$f4f3be4052bd1b48d39388f8052f1d5495bf3233",  
	"basename": "output.txt",  
	"location": "file:///Users/adinasarapu/cwl/output.txt",  
	"path": "/Users/adinasarapu/cwl/output.txt",  
	"class": "File",  
	"size": 3257  
	}  
}  
INFO Final process status is success  

`$ cat output.txt`

Imported emp-single-end-sequences as EMPSingleEndDirFmt to emp-single-end-sequences.qza  
Saved SampleData[SequencesWithQuality] to: demux.qza  
Saved ErrorCorrectionDetails to: error.qza  
Saved Visualization to: demux.qzv  
Saved FeatureTable[Frequency] to: table.qza  
Saved FeatureData[Sequence] to: rep-seqs.qza  
Saved SampleData[DADA2Stats] to: stats-dada.qza  
Saved Visualization to: stats-dada.qzv  
Saved Visualization to: table.qzv  
Saved Visualization to: rep-seqs.qzv  
Saved FeatureData[AlignedSequence] to: aligned-rep-seqs.qza  
Saved FeatureData[AlignedSequence] to: masked-aligned-rep-seqs.qza  
Saved Phylogeny[Unrooted] to: unrooted-tree.qza  
Saved Phylogeny[Rooted] to: rooted-tree.qza  
Saved FeatureTable[Frequency] to: core-metrics-results/rarefied_table.qza  
Saved SampleData[AlphaDiversity] % Properties('phylogenetic') to: core-metrics-results/faith_pd_vector.qza  
Saved SampleData[AlphaDiversity] to: core-metrics-results/observed_otus_vector.qza  
Saved SampleData[AlphaDiversity] to: core-metrics-results/shannon_vector.qza  
Saved SampleData[AlphaDiversity] to: core-metrics-results/evenness_vector.qza  
Saved DistanceMatrix % Properties('phylogenetic') to: core-metrics-results/unweighted_unifrac_distance_matrix.qza  
Saved DistanceMatrix % Properties('phylogenetic') to: core-metrics-results/weighted_unifrac_distance_matrix.qza  
Saved DistanceMatrix to: core-metrics-results/jaccard_distance_matrix.qza  
Saved DistanceMatrix to: core-metrics-results/bray_curtis_distance_matrix.qza  
Saved PCoAResults to: core-metrics-results/unweighted_unifrac_pcoa_results.qza  
Saved PCoAResults to: core-metrics-results/weighted_unifrac_pcoa_results.qza  
Saved PCoAResults to: core-metrics-results/jaccard_pcoa_results.qza  
Saved PCoAResults to: core-metrics-results/bray_curtis_pcoa_results.qza  
Saved Visualization to: core-metrics-results/unweighted_unifrac_emperor.qzv  
Saved Visualization to: core-metrics-results/weighted_unifrac_emperor.qzv  
Saved Visualization to: core-metrics-results/jaccard_emperor.qzv  
Saved Visualization to: core-metrics-results/bray_curtis_emperor.qzv  
Saved Visualization to: core-metrics-results/faith-pd-group-significance.qzv  
Saved Visualization to: core-metrics-results/evenness-group-significance.qzv  
Saved Visualization to: core-metrics-results/unweighted-unifrac-body-site-significance.qzv  
Saved Visualization to: core-metrics-results/unweighted-unifrac-subject-group-significance.qzv  
Saved Visualization to: core-metrics-results/unweighted-unifrac-emperor-DaysSinceExperimentStart.qzv  
Saved Visualization to: core-metrics-results/bray-curtis-emperor-DaysSinceExperimentStart.qzv  
Saved Visualization to: alpha-rarefaction.qzv  
Imported 85_otus.fasta as DNASequencesDirectoryFormat to 85_otus.qza  
Imported 85_otu_taxonomy.txt as HeaderlessTSVTaxonomyFormat to train-ref-taxonomy.qza  
Saved FeatureData[Sequence] to: train-ref-seqs.qza  
Saved TaxonomicClassifier to: classifier.qza  
Saved FeatureData[Taxonomy] to: taxonomy.qza  
Saved Visualization to: taxonomy.qzv  
Saved Visualization to: taxa-bar-plots.qzv  
Saved FeatureTable[Composition] to: comp-table.qza  
Saved Visualization to: ancom-Subject.qzv  
Saved FeatureTable[Frequency] to: table-l6.qza  
Saved FeatureTable[Composition] to: comp-table-l6.qza  
Saved Visualization to: l6-ancom-subject.qzv  
