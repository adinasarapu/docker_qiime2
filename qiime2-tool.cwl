#!/usr/bin/env cwl-runner

cwlVersion: v1.0

class: CommandLineTool

baseCommand: sh

stdout: output.txt

inputs:
  my_script:
    type: File
    inputBinding:
      position: 1
outputs:
  std_out:
    type: stdout
