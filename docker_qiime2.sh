#!/bin/bash

# https://docs.qiime2.org/2019.10/install/virtual/docker/

# Ashok R. Dinasarapu Ph.D			
# Emory University, Dept. of Human Genetics	
# ashok.reddy.dinasarapu@emory.edu		
# Date: 12/31/2019				

# To install QIIME 2 using Docker
# First install Docker hub and login, then run the following command

# Download QIIME 2 Image
# docker pull qiime2/core:2019.10

# Confirm the installation
# docker run -t -i -v $(pwd):/data qiime2/core:2019.10 qiime

# If you want to start QIIME2 an as interactive mode
# docker run -it qiime2/core:2019.10

# Pass the entire argument to /bin/bash
# docker run image /bin/bash -c "cd /some/path && python a.py"
# docker run image /bin/bash -c "cd /path/to/somewhere; python a.py"

# Data download 
# Data import into QIIME2
# Demultiplex sequence data
# Summarize counts per sample for all samples, and generate interactive positional quality plots based on `n` randomly selected sequences

# spin up a new container for each command
# by default (without neither -i nor -t options) a Docker container only sends its output to STDOUT,
# with -i option comes connection to STDIN,
# -t option pulls in a terminal interface driver, that works on top of STDIN/STDOUT. 

CONTAINER="docker run --rm -i -u $(id -u):$(id -g) -v $(pwd):/data qiime2/core:2019.10"

$CONTAINER  mkdir emp-single-end-sequences && \
  wget -O "sample-metadata.tsv" "https://data.qiime2.org/2019.10/tutorials/moving-pictures/sample_metadata.tsv" && \
  wget -O "emp-single-end-sequences/sequences.fastq.gz" "https://data.qiime2.org/2019.10/tutorials/moving-pictures/emp-single-end-sequences/sequences.fastq.gz" && \
  wget -O "emp-single-end-sequences/barcodes.fastq.gz" "https://data.qiime2.org/2019.10/tutorials/moving-pictures/emp-single-end-sequences/barcodes.fastq.gz"

$CONTAINER qiime tools import \
  --type EMPSingleEndSequences \
  --input-path emp-single-end-sequences \
  --output-path emp-single-end-sequences.qza

$CONTAINER qiime demux emp-single \
  --i-seqs emp-single-end-sequences.qza \
  --m-barcodes-file sample-metadata.tsv \
  --m-barcodes-column barcode-sequence \
  --o-per-sample-sequences demux.qza \
  --o-error-correction-details error.qza  

$CONTAINER qiime demux summarize \
  --i-data demux.qza \
  --o-visualization demux.qzv

# Denoises single-end sequences, dereplicates them, and filters chimeras
# Generate a tabular view of Metadata.
# Generate visual and tabular summaries of a feature table
# Generate tabular view of feature identifier to sequence mapping, including links to BLAST each sequence against the NCBI nt database

$CONTAINER qiime dada2 denoise-single \
  --i-demultiplexed-seqs demux.qza \
  --p-trim-left 0 \
  --p-trunc-len 120 \
  --o-representative-sequences rep-seqs.qza \
  --o-table table.qza \
  --o-denoising-stats stats-dada.qza

$CONTAINER qiime metadata tabulate \
  --m-input-file stats-dada.qza \
  --o-visualization stats-dada.qzv

$CONTAINER qiime feature-table summarize \
  --i-table table.qza \
  --o-visualization table.qzv \
  --m-sample-metadata-file sample-metadata.tsv

$CONTAINER qiime feature-table tabulate-seqs \
  --i-data rep-seqs.qza \
  --o-visualization rep-seqs.qzv

# Diversity
# Sequence alignment using MAFFT, the resulting masked alignment will be used to infer a phylogenetic tree and then subsequently rooted at its midpoint
# Applies a collection of diversity metrics (both phylogenetic and non-phylogenetic) to a feature table
# Visually and statistically compare groups of alpha diversity values
# Determine whether groups of samples are significantly different from one another using a permutation-based statistical test
# Generates an interactive ordination plot where the user can visually integrate sample metadata

$CONTAINER qiime phylogeny align-to-tree-mafft-fasttree \
  --i-sequences rep-seqs.qza \
  --o-alignment aligned-rep-seqs.qza \
  --o-masked-alignment masked-aligned-rep-seqs.qza \
  --o-tree unrooted-tree.qza \
  --o-rooted-tree rooted-tree.qza

$CONTAINER qiime diversity core-metrics-phylogenetic \
  --i-phylogeny rooted-tree.qza \
  --i-table table.qza \
  --p-sampling-depth 1109 \
  --m-metadata-file sample-metadata.tsv \
  --output-dir core-metrics-results

$CONTAINER qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/faith_pd_vector.qza \
  --m-metadata-file sample-metadata.tsv \
  --o-visualization core-metrics-results/faith-pd-group-significance.qzv

$CONTAINER qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/evenness_vector.qza \
  --m-metadata-file sample-metadata.tsv \
  --o-visualization core-metrics-results/evenness-group-significance.qzv

$CONTAINER qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file sample-metadata.tsv \
  --m-metadata-column body-site \
  --o-visualization core-metrics-results/unweighted-unifrac-body-site-significance.qzv \
  --p-pairwise

$CONTAINER qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file sample-metadata.tsv \
  --m-metadata-column subject \
  --o-visualization core-metrics-results/unweighted-unifrac-subject-group-significance.qzv \
  --p-pairwise

$CONTAINER qiime emperor plot \
  --i-pcoa core-metrics-results/unweighted_unifrac_pcoa_results.qza \
  --m-metadata-file sample-metadata.tsv \
  --p-custom-axes days-since-experiment-start \
  --o-visualization core-metrics-results/unweighted-unifrac-emperor-DaysSinceExperimentStart.qzv

$CONTAINER qiime emperor plot \
  --i-pcoa core-metrics-results/bray_curtis_pcoa_results.qza \
  --m-metadata-file sample-metadata.tsv \
  --p-custom-axes days-since-experiment-start \
  --o-visualization core-metrics-results/bray-curtis-emperor-DaysSinceExperimentStart.qzv

# Rarefaction
# Generate interactive alpha rarefaction curves by computing rarefactions between `min_depth` and `max_depth`

$CONTAINER qiime diversity alpha-rarefaction \
  --i-table table.qza \
  --i-phylogeny rooted-tree.qza \
  --p-max-depth 4000 \
  --m-metadata-file sample-metadata.tsv \
  --o-visualization alpha-rarefaction.qzv

# Taxonomy
# Data download 
    
$CONTAINER wget -O "85_otus.fasta" "https://data.qiime2.org/2019.10/tutorials/training-feature-classifiers/85_otus.fasta"
$CONTAINER wget -O "85_otu_taxonomy.txt" "https://data.qiime2.org/2019.10/tutorials/training-feature-classifiers/85_otu_taxonomy.txt"

$CONTAINER qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path 85_otus.fasta \
  --output-path 85_otus.qza

$CONTAINER qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path 85_otu_taxonomy.txt \
  --output-path train-ref-taxonomy.qza

$CONTAINER qiime feature-classifier extract-reads \
  --i-sequences 85_otus.qza \
  --p-f-primer GTGCCAGCMGCCGCGGTAA \
  --p-r-primer GGACTACHVGGGTWTCTAAT \
  --p-trunc-len 120 \
  --p-min-length 100 \
  --p-max-length 400 \
  --o-reads train-ref-seqs.qza

$CONTAINER qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads train-ref-seqs.qza \
  --i-reference-taxonomy train-ref-taxonomy.qza \
  --o-classifier classifier.qza
    
# $CONTAINER wget -O "gg-13-8-99-515-806-nb-classifier.qza" "https://data.qiime2.org/2018.11/common/gg-13-8-99-515-806-nb-classifier.qza"

# Classify reads by taxon using a fitted classifier
# Generate a tabular view of Metadata
# Produces an interactive barplot visualization of taxonomies

$CONTAINER qiime feature-classifier classify-sklearn \
  --i-classifier classifier.qza \
  --i-reads rep-seqs.qza \
  --o-classification taxonomy.qza

$CONTAINER qiime metadata tabulate \
  --m-input-file taxonomy.qza \
  --o-visualization taxonomy.qzv
	
$CONTAINER qiime taxa barplot \
  --i-table table.qza \
  --i-taxonomy taxonomy.qza \
  --m-metadata-file sample-metadata.tsv \
  --o-visualization taxa-bar-plots.qzv

# Composition
# Filter samples from table based on frequency and/or metadata.
# Increment all counts in table by pseudocount
# Apply Analysis of Composition of Microbiomes (ANCOM) to identify features that are differentially abundant across groups
# Collapse groups of features that have the same taxonomic assignment through the specified level.

#$CONTAINER qiime feature-table filter-samples \
#  --i-table table.qza \
#  --m-metadata-file sample-metadata.tsv \
#  --p-where "body-site='gut'" \
#  --o-filtered-table gut-table.qza

$CONTAINER qiime composition add-pseudocount \
  --i-table table.qza \
  --o-composition-table comp-table.qza
        
$CONTAINER qiime composition ancom \
  --i-table comp-table.qza \
  --m-metadata-file sample-metadata.tsv \
  --m-metadata-column subject \
  --o-visualization ancom-Subject.qzv
	
$CONTAINER qiime taxa collapse \
  --i-table table.qza \
  --i-taxonomy taxonomy.qza \
  --p-level 6 \
  --o-collapsed-table table-l6.qza
	
$CONTAINER qiime composition add-pseudocount \
  --i-table table-l6.qza \
  --o-composition-table comp-table-l6.qza
	
$CONTAINER qiime composition ancom \
  --i-table comp-table-l6.qza \
  --m-metadata-file sample-metadata.tsv \
  --m-metadata-column subject \
  --o-visualization l6-ancom-subject.qzv
